package com.andreww.hale.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.andreww.hale.R;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Author: Andrew Wagner
 * Project: Hale v2
 * Date: 8/21/13
 * Time: 11:34 PM
 */
public class Signup extends Activity {
    protected EditText mUsername, mPassword, mEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        mUsername = (EditText) findViewById(R.id.username_fet);
        mPassword = (EditText) findViewById(R.id.password_fet);
        mEmail = (EditText) findViewById(R.id.email_fet);
    }

    public void signUpClick(View v) {
        ParseUser user = new ParseUser();
        user.setUsername(mUsername.getText().toString());
        user.setPassword(mPassword.getText().toString());
        user.setEmail(mEmail.getText().toString());
        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null) {
                    Intent launchMain = new Intent(Signup.this, Main.class);
                    startActivity(launchMain);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
