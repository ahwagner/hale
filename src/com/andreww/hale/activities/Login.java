package com.andreww.hale.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.andreww.hale.R;
import com.andreww.hale.utils.SharedPrefUtils;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * Author: Andrew Wagner
 * Project: Hale v2
 * Date: 8/18/13
 * Time: 11:58 PM
 */
public class Login extends Activity {

    protected static String PARSE_USER_KEY = "com.andreww.hale.PARSE_USER";

    protected LinearLayout mLayout;
    protected EditText mUsernameET;
    protected EditText mPasswordET;
    protected Button mLogin;
    protected Button mSignUp;
    protected ImageView mLogo;
    protected TextView mLabel;
    protected TextView mMessage;
    protected ImageView mIndicator;
    protected ProgressBar mLoading;

    protected ParseUser mUser;
    protected String mUsername;
    protected String mPassword;

    protected int mBackPressCounter = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        mUser = new ParseUser();

        mLayout = (LinearLayout) findViewById(R.id.layout);
        mUsernameET = (EditText) findViewById(R.id.username_et);
        mPasswordET = (EditText) findViewById(R.id.password_et);
        mLogin = (Button) findViewById(R.id.log_in_button);
        mSignUp = (Button) findViewById(R.id.sign_up_button);
        mLogo = (ImageView) findViewById(R.id.logo);
        mLabel = (TextView) findViewById(R.id.label);

        mMessage = new TextView(this);
        LinearLayout.LayoutParams messageParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        messageParams.setMargins(0, 4, 0, 32);
        mMessage.setLayoutParams(messageParams);
        mMessage.setGravity(Gravity.CENTER_HORIZONTAL);
        mMessage.setTextSize(18);
        mMessage.setTypeface(Typeface.create("sans-serif-light", 0));

        mIndicator = new ImageView(this);

        mLoading = new ProgressBar(this);
        mLoading.setIndeterminate(true);

        showKeyboard(mUsernameET);
    }

    @Override
    public void onBackPressed() {
        mBackPressCounter = (mBackPressCounter + 1);
        if(mBackPressCounter >= 2) android.os.Process.killProcess(android.os.Process.myPid());
    }

    public void logInClick(View v) {
        mUsername = mUsernameET.getText().toString();
        mPassword = mPasswordET.getText().toString();

        hideKeyboard(this.getCurrentFocus().getWindowToken());

        animate(4);

        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                mUser.logInInBackground(mUsername, mPassword, new LogInCallback() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {
                        if (e == null && parseUser != null) {
                            success();
                        } else if (parseUser == null) {
                            invalid();
                        } else {
                            error();
                        }
                    }
                });
            }
        }, 2500);
    }

    public void success() {
        animate(1);
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPrefUtils.setUserLoggedIn(getApplicationContext());
                Intent launchMain = new Intent(Login.this, Main.class);
                startActivity(launchMain);
                finish();
            }
        }, 3000);
    }

    public void invalid() {
        animate(2);
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                animate(0);
            }
        }, 3000);
    }

    public void error() {
        animate(3);
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                animate(0);
            }
        }, 3000);
    }

    public void animate(int code) {
        if(code == 0) {
            mLayout.removeViewAt(0);
            mLayout.addView(mLogo, 0);
            mLayout.removeViewAt(1);
            mLayout.addView(mLabel, 1);
            mUsernameET.setEnabled(true);
            mPasswordET.setEnabled(true);
            mLogin.setEnabled(true);
            mSignUp.setEnabled(true);
        } else if(code == 1) {
            mIndicator.setImageResource(R.drawable.ic_action_tick_green_dark);
            mMessage.setText(getResources().getString(R.string.login_success));
            mLayout.removeViewAt(0);
            mLayout.addView(mIndicator, 0);
            mLayout.removeViewAt(1);
            mLayout.addView(mMessage, 1);
            mUsernameET.setEnabled(false);
            mPasswordET.setEnabled(false);
            mLogin.setEnabled(false);
            mSignUp.setEnabled(false);
        } else if(code == 2) {
            mIndicator.setImageResource(R.drawable.ic_action_warning_red_dark);
            mMessage.setText(getResources().getString(R.string.login_error_invalid_field));
            mLayout.removeViewAt(0);
            mLayout.addView(mIndicator, 0);
            mLayout.removeViewAt(1);
            mLayout.addView(mMessage, 1);
            mUsernameET.setEnabled(false);
            mPasswordET.setEnabled(false);
            mLogin.setEnabled(false);
            mSignUp.setEnabled(false);
        } else if(code == 3) {
            mIndicator.setImageResource(R.drawable.ic_action_warning_red_dark);
            mMessage.setText(getResources().getString(R.string.login_error_unknown));
            mLayout.removeViewAt(0);
            mLayout.addView(mIndicator, 0);
            mLayout.removeViewAt(1);
            mLayout.addView(mMessage, 1);
            mUsernameET.setEnabled(false);
            mPasswordET.setEnabled(false);
            mLogin.setEnabled(false);
            mSignUp.setEnabled(false);
        } else if(code == 4) {
            mMessage.setText(getResources().getString(R.string.loading));
            mLayout.removeViewAt(0);
            mLayout.addView(mLoading, 0);
            mLayout.removeViewAt(1);
            mLayout.addView(mMessage, 1);
            mUsernameET.setEnabled(false);
            mPasswordET.setEnabled(false);
            mLogin.setEnabled(false);
            mSignUp.setEnabled(false);
        }
    }

    public void signUpClick(View v) {
        Intent i = new Intent(this, Signup.class);
        startActivity(i);
    }

    public void showKeyboard(View v) {
        if (v.requestFocus()) {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public void hideKeyboard(IBinder window) {
        InputMethodManager inputManager = (InputMethodManager)
                this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(window,
                InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
