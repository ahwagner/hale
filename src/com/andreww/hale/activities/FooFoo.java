package com.andreww.hale.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.Toast;
import com.andreww.hale.R;
import com.parse.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Andrew Wagner
 * Project: Hale
 * Date: 8/25/13
 * Time: 2:22 AM
 */
public class FooFoo extends Activity {

    public static final String KCAL_KEY = "Energ_Kcal";
    public static final String GMWT1_KEY = "GmWt_1";
    public static final String GMWT2_KEY = "GmWt_2";
    public static final String GMWT1_DESC_KEY = "GmWt_Desc1";
    public static final String GMWT2_DESC_KEY = "GmWt_Desc2";
    public static final String DATABASE_NUM_KEY = "NDB_No";
    public static final String REFUSE_KEY = "Refuse_Pct";
    public static final String DESC_KEY = "Shrt_Desc";
    public static final String VIT_A1_KEY = "Vit_A_IU";
    public static final String VIT_A2_KEY = "Vit_A_RAE";
    public static final String ALPHA_CAROT_KEY = "alphaCarotG";
    public static final String ASH_KEY = "ashG";
    public static final String BETA_CAROT_KEY = "betaCarotG";
    public static final String BETA_CRYPT_KEY = "betaCryptG";
    public static final String CALCIUM_KEY = "calciumMg";
    public static final String CARB_KEY = "carbohydrtG";
    public static final String CHOLESTRL_KEY = "cholestrlMg";
    public static final String CHOLINE_KEY = "cholineTot_mg";
    public static final String COPPER_KEY = "copperMg";
    public static final String FAMONO_KEY = "faMonoG";
    public static final String FAPOLY_KEY = "faPolyG";
    public static final String FASAT_KEY = "faSatG";
    public static final String FIBER_KEY = "fiberTdG";
    public static final String FOLATE1_KEY = "folateDfeG";
    public static final String FOLATE2_KEY = "folateTotG";
    public static final String FOLICACID_KEY = "folicAcidG";
    public static final String FOODFOLATE_KEY = "foodFolateG";
    public static final String IRON_KEY = "ironMg";
    public static final String USER_ENTRY_KEY = "isUserEntry";
    public static final String LIPID_KEY = "lipidTotG";
    public static final String LUTZEA_KEY = "lutZea_g";
    public static final String LYCOPENE_KEY = "lycopeneG";
    public static final String MAGNESIUM_KEY = "magnesiumMg";
    public static final String MANGANESE_KEY = "manganeseMg";
    public static final String NIACIN_KEY = "niacinMg";
    public static final String PANTO_KEY = "pantoAcidMg";
    public static final String PHOSPHORUS_KEY = "phosphorusMg";
    public static final String POTASSIUM_KEY = "potassiumMg";
    public static final String PROTEIN_KEY = "proteinG";
    public static final String RETINOL_KEY = "retinolG";
    public static final String RIBO_KEY = "riboflavinMg";
    public static final String SELENIUM_KEY = "seleniumG";
    public static final String SODIUM_KEY = "sodiumMg";
    public static final String SUGARTOT_KEY = "sugarTotG";
    public static final String THAMIN_KEY = "thiaminMg";
    public static final String VITB12_KEY = "vitB12G";
    public static final String VITB6_KEY = "vitB6Mg";
    public static final String VITC_KEY = "vitCMg";
    public static final String VITD1_KEY = "vitDG";
    public static final String VITD2_KEY = "vitDIu";
    public static final String VITE_KEY = "vitEMg";
    public static final String VITK_KEY = "vitKG";
    public static final String WATER_KEY = "waterG";
    public static final String ZINC_KEY = "zincMg";

    public ProgressDialog mProgress;
    public List<ParseObject> mParseList;
    public Button mButton;
    int mTimesSaved = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.foo_foo);

        mButton = (Button) findViewById(R.id.sendjson);

        mProgress = new ProgressDialog(this);
        queryCurrentDb(0);
    }

    public void queryCurrentDb(final int skip) {
        mProgress.setTitle("Querying USDADB");
        mProgress.setMessage("Loading...");
        mProgress.setCancelable(false);
        if(!mProgress.isShowing()) mProgress.show();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("USDADB");
        query.setLimit(1000);
        query.setSkip(skip);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if(mParseList == null) {
                    mParseList = parseObjects;
                } else {
                    mParseList.addAll(parseObjects);
                }
                if(skip != 8000) {
                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            queryCurrentDb(skip + 1000);
                        }
                    }, 20000);
                    Toast.makeText(getApplicationContext(), "Skipping to " + String.valueOf(skip + 1000), Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "mParseList: " + String.valueOf(mParseList.size()), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "mParseList: " + String.valueOf(mParseList.size()), Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "Cooling down main thread", Toast.LENGTH_SHORT).show();
                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mProgress.dismiss();
                            mProgress.setTitle("Parsing JSON");
                            mProgress.show();
                            doJSONParsing();
                        }
                    }, 20000);
                }
            }
        });
    }
    public void doJSONParsing() {
        final List<JSONObject> jsonObjectList = new ArrayList<JSONObject>();
        for(int index = 0; index < mParseList.size(); index++) {
            ParseObject parseObject = mParseList.get(index);
            JSONObject jsonObject = new JSONObject();
            put(jsonObject, parseObject, DATABASE_NUM_KEY);
            put(jsonObject, parseObject, DESC_KEY);
            put(jsonObject, parseObject, USER_ENTRY_KEY);
            put(jsonObject, parseObject, WATER_KEY);
            put(jsonObject, parseObject, KCAL_KEY);
            put(jsonObject, parseObject, PROTEIN_KEY);
            put(jsonObject, parseObject, LIPID_KEY);
            put(jsonObject, parseObject, ASH_KEY);
            put(jsonObject, parseObject, CARB_KEY);
            put(jsonObject, parseObject, FIBER_KEY);
            put(jsonObject, parseObject, SUGARTOT_KEY);
            put(jsonObject, parseObject, CALCIUM_KEY);
            put(jsonObject, parseObject, IRON_KEY);
            put(jsonObject, parseObject, MAGNESIUM_KEY);
            put(jsonObject, parseObject, PHOSPHORUS_KEY);
            put(jsonObject, parseObject, POTASSIUM_KEY);
            put(jsonObject, parseObject, SODIUM_KEY);
            put(jsonObject, parseObject, ZINC_KEY);
            put(jsonObject, parseObject, COPPER_KEY);
            put(jsonObject, parseObject, MANGANESE_KEY);
            put(jsonObject, parseObject, SELENIUM_KEY);
            put(jsonObject, parseObject, VITC_KEY);
            put(jsonObject, parseObject, THAMIN_KEY);
            put(jsonObject, parseObject, RIBO_KEY);
            put(jsonObject, parseObject, NIACIN_KEY);
            put(jsonObject, parseObject, PANTO_KEY);
            put(jsonObject, parseObject, VITB6_KEY);
            put(jsonObject, parseObject, FOLATE1_KEY);
            put(jsonObject, parseObject, FOLICACID_KEY);
            put(jsonObject, parseObject, FOLATE2_KEY);
            put(jsonObject, parseObject, FOODFOLATE_KEY);
            put(jsonObject, parseObject, CHOLINE_KEY);
            put(jsonObject, parseObject, VITB12_KEY);
            put(jsonObject, parseObject, VIT_A1_KEY);
            put(jsonObject, parseObject, VIT_A2_KEY);
            put(jsonObject, parseObject, RETINOL_KEY);
            put(jsonObject, parseObject, ALPHA_CAROT_KEY);
            put(jsonObject, parseObject, BETA_CAROT_KEY);
            put(jsonObject, parseObject, BETA_CRYPT_KEY);
            put(jsonObject, parseObject, LYCOPENE_KEY);
            put(jsonObject, parseObject, LUTZEA_KEY);
            put(jsonObject, parseObject, VITE_KEY);
            put(jsonObject, parseObject, VITD1_KEY);
            put(jsonObject, parseObject, VITD2_KEY);
            put(jsonObject, parseObject, VITK_KEY);
            put(jsonObject, parseObject, FASAT_KEY);
            put(jsonObject, parseObject, FAMONO_KEY);
            put(jsonObject, parseObject, FAPOLY_KEY);
            put(jsonObject, parseObject, CHOLESTRL_KEY);
            put(jsonObject, parseObject, GMWT1_KEY);
            put(jsonObject, parseObject, GMWT1_DESC_KEY);
            put(jsonObject, parseObject, GMWT2_KEY);
            put(jsonObject, parseObject, GMWT2_DESC_KEY);
            put(jsonObject, parseObject, REFUSE_KEY);
            jsonObjectList.add(jsonObject);
        }
        Toast.makeText(getApplicationContext(), "Cooling down main thread", Toast.LENGTH_SHORT).show();
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                mProgress.dismiss();
                mProgress.setTitle("Sending JSON To Hale");
                mProgress.show();
                Toast.makeText(getApplicationContext(), "mJSONObjectList: " + String.valueOf(jsonObjectList.size()), Toast.LENGTH_SHORT).show();
                saveJSONToServer(jsonObjectList, 0);
            }
        }, 20000);
    }

    public void put(JSONObject obj, ParseObject po, String key) {
        try{
            if(key.equals(DESC_KEY) || key.equals(GMWT1_DESC_KEY) || key.equals(GMWT2_DESC_KEY)) {
                obj.put(key, po.getString(key));
            } else if(key.equals(USER_ENTRY_KEY)) {
                obj.put(key, po.getBoolean(key));
            } else {
                obj.put(key, po.getInt(key));
            }
        } catch(JSONException e) {
        }
    }

    public void saveJSONToServer(final List<JSONObject> jsonObjects, final int skip) {
        final List<ParseObject> objects = new ArrayList<ParseObject>();
        if(skip == 0) {
            for(int i = 0; i < jsonObjects.size(); i++) {
                ParseObject object = new ParseObject("HaleMaster");
                object.put("NutritionDb", jsonObjects.get(i));
                objects.add(object);
            }
        }
        int endpoint;
        if(skip != 8000) {
            endpoint = skip + 100;
        } else {
            endpoint = objects.size();
        }
        ParseObject.saveAllInBackground(objects.subList(skip, endpoint), new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    if (skip != 8000) {
                        Toast.makeText(getApplicationContext(), "Pushed " + String.valueOf(skip), Toast.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "Cooling down main thread", Toast.LENGTH_SHORT).show();
                        Handler h = new Handler();
                        h.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Saving " + String.valueOf(skip + 500), Toast.LENGTH_SHORT).show();
                                saveJSONToServer(jsonObjects, skip + 100);
                            }
                        }, 20000);
                    } else {
                        mProgress.dismiss();
                        Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
