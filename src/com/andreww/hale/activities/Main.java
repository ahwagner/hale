package com.andreww.hale.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.afollestad.silk.activities.SilkDrawerActivity;
import com.andreww.hale.R;
import com.andreww.hale.fragments.Nutrition;
import com.andreww.hale.objects.FoObject;
import com.andreww.hale.objects.NutritionEntry;
import com.andreww.hale.utils.SharedPrefUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Main extends SilkDrawerActivity {
    public static final String TAG = Main.class.getSimpleName();
    public LinearLayout mDrawerLayout;
    public ListView mDrawerList;
    public String[] mDrawerItems;
    public String[] mFragClassNames;
    public int mSelectedFragmentPosition;
    public Fragment mSelectedFragmentType;
    public List<ParseObject> mCombinedQueryList;
    public List<JSONObject> mNutritionLogList = new ArrayList<JSONObject>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!SharedPrefUtils.isUserLoggedIn(this)) {
            Log.d(TAG, "User not logged in; launching Login; finishing Main");
            Intent launchLogin = new Intent(Main.this, Login.class);
            startActivity(launchLogin);
            finish();
        }
        //buildFooData();
        doCombinedQuery();
        Log.d(TAG, "User logged in; continuing Main");
    }

    @Override
    protected void onResume() {
        super.onResume();

        mDrawerLayout = (LinearLayout) findViewById(R.id.drawer_inner_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mDrawerItems = getResources().getStringArray(R.array.drawer_items);
        mFragClassNames = getResources().getStringArray(R.array.frag_class_names);
        ArrayAdapter<String> drawerAdapter = new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mDrawerItems);
        mDrawerList.setAdapter(drawerAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        selectItem(0);
    }

    @Override
    public int getLayout() {
        return(R.layout.main);
    }

    @Override
    public DrawerLayout getDrawerLayout() {
        return (DrawerLayout) findViewById(R.id.drawer_layout);
    }

    @Override
    public int getOpenedTextRes() {
        return(R.string.app_name);
    }

    @Override
    public int getDrawerIndicatorRes() {
        return(R.drawable.ic_drawer);
    }

    @Override
    public int getDrawerShadowRes() {
        return(R.drawable.drawer_shadow);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
            mSelectedFragmentPosition = position;
        }

    }

    private void selectItem(int pos) {

        if(pos == 0) {
            mSelectedFragmentType = new Nutrition();
        }
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.content_frame, mSelectedFragmentType).commit();

        mDrawerList.setItemChecked(pos, true);
        setTitle(mDrawerItems[pos]);
        getDrawerLayout().closeDrawer(mDrawerLayout);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        buildOptionsMenu(mSelectedFragmentPosition, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(isDrawerOpen()) {
            menu.setGroupVisible(R.id.hide_on_drawer_open, false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_settings:
                Toast.makeText(this, getResources().getString(R.string.not_implemented), Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_new_nutrition_entry:
                Nutrition nutrition = (Nutrition) mSelectedFragmentType;
                nutrition.launchNewNutritionEntryDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static void buildOptionsMenu(int fragCase, Menu menu) {
        switch(fragCase) {
            case 0:
                menu.findItem(R.id.action_new_nutrition_entry).setVisible(true);
            default:
                break;
        }
    }

    public void buildFooData() {
        for(int i = 0; i <= 5; i++) {
            ParseObject object = new ParseObject("CombinedQueryTest");
            FoObject entry = new FoObject("Test", i);
            object.put("NutritionLogs", entry);
            object.saveInBackground();
        }
        for(int i = 0; i <= 3; i ++ ) {
            ParseObject object = new ParseObject("CombinedQueryTest");
            object.put("FooText", "sample" + String.valueOf(i));
            object.saveInBackground();
        }
    }
    public static ParseQuery<NutritionEntry> getCachedSearch() {
        final ParseQuery<NutritionEntry> query = ParseQuery.getQuery(NutritionEntry.class);
        query.setMaxCacheAge(86400000);
        if(query.hasCachedResult()) {
            query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ONLY);
        } else {
            query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
        }
        return query;
    }

    public void doCombinedQuery() {
        final ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("CombinedQueryTest");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                mCombinedQueryList =  parseObjects;
                buildNutritionLogList();
                Toast.makeText(getApplicationContext(), String.valueOf(getNutritionLogList().size()), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void buildNutritionLogList() {
        for(int index = 0; index < mCombinedQueryList.size(); index++) {
            ParseObject object = mCombinedQueryList.get(index);
            if(object.getJSONObject("NutritionLogs") != null) {
                mNutritionLogList.add(object.getJSONObject("NutritionLogs"));
            }
        }
    }

    public List<JSONObject> getNutritionLogList() {
        return mNutritionLogList;
    }
}
