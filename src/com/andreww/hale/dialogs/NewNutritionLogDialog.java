package com.andreww.hale.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import com.andreww.hale.R;

/**
 * Author: Andrew Wagner
 * Project: Hale v2
 * Date: 8/22/13
 * Time: 1:06 AM
 */
public class NewNutritionLogDialog extends AlertDialog.Builder {
    public Context mContext;
    public EditText mDesc, mServingsConsumed, mServing, mCalories, mProtein;

    public NewNutritionLogDialog(Context context) {
        super(context);
        mContext = context;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.new_nutrition_log_dialog, null);
        setView(view);
        setTitle("New Nutrition Entry");

        mDesc = (EditText)  view.findViewById(R.id.desc_et);
        mServingsConsumed = (EditText) view.findViewById(R.id.servings_consumed_et);
        mServing = (EditText) view.findViewById(R.id.serving_et);
        mCalories = (EditText) view.findViewById(R.id.calories_et);
        mProtein = (EditText) view.findViewById(R.id.protein_et);
        setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }

    public String getDesc() {
        return mDesc.getText().toString();
    }

    public int getServingsConsumed() {
        if(mServingsConsumed.getText().toString().equals("") || mServingsConsumed.getText().toString() == null) {
            return -1;
        } else {
            return Integer.parseInt(mServingsConsumed.getText().toString());
        }
    }

    public String getServing() {
        return mServing.getText().toString();
    }

    public int getCalories() {
        if(mCalories.getText().toString().equals("") || mCalories.getText().toString() == null) {
            return -1;
        } else {
            return Integer.parseInt(mCalories.getText().toString());
        }
    }

    public int getProtein() {
        if(mProtein.getText().toString().equals("") || mProtein.getText().toString() == null) {
            return -1;
        } else {
            return Integer.parseInt(mProtein.getText().toString());
        }
    }
}
