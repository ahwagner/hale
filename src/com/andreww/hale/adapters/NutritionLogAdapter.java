package com.andreww.hale.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.andreww.hale.R;
import com.andreww.hale.objects.NutritionLogEntry;
import com.andreww.hale.utils.WordUtils;

import java.util.List;

/**
 * Author: Andrew Wagner
 * Project: Hale v2
 * Date: 8/20/13
 * Time: 10:48 PM
 */
public class NutritionLogAdapter extends ArrayAdapter<NutritionLogEntry> {
    protected Context mContext;
    protected List<NutritionLogEntry> mEntries;
    protected static NutritionLogEntry mEntry;
    protected boolean mExpanded = false;
    protected boolean mFirstExpand = true;
    protected int mExpandedPos;
    protected ViewStub mStub;

    public NutritionLogAdapter(Context context, List<NutritionLogEntry> objects) {
        super(context, R.layout.nutrition_card_item, objects);
        this.mContext = context;
        this.mEntries = objects;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        if(view == null){
            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = inflater.inflate(R.layout.nutrition_card_item, null);
        }

        mEntry = mEntries.get(position);
        final LinearLayout card = (LinearLayout) view.findViewById(R.id.card);
        final ViewStub stub = (ViewStub) view.findViewById(R.id.stub);
        final TextView desc = (TextView) view.findViewById(R.id.desc);
        final TextView serving = (TextView) view.findViewById(R.id.serving);
        final TextView calories = (TextView) view.findViewById(R.id.calories);
        final TextView protein = (TextView) view.findViewById(R.id.protein);
        final TextView noDetails = (TextView) view.findViewById(R.id.no_details_available);
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**
                    On first click, or "first expand," show the expansion, set expanded true, set the current expansion
                    as the current expansion, set the saved expanded position as the position at the time of this click,
                    and set first expand false
                */
                if(mFirstExpand) {
                    stub.setVisibility(View.VISIBLE);
                    mExpanded = true;
                    mStub = stub;
                    mExpandedPos = position;
                    mFirstExpand = false;

                /**
                    If a card is expanded and the saved expanded position is the same as the position at the time of this click,
                    hide the expansion, and set expanded false
                */
                } else if(mExpanded && mExpandedPos == position) {
                    stub.setVisibility(View.GONE);
                    mExpanded = false;

                /**
                    If a card is not expanded but the saved expanded position is still the same as the position at the time
                    of this click, show the expansion, and set expanded true
                 */
                } else if(!mExpanded && mExpandedPos == position) {
                    stub.setVisibility(View.VISIBLE);
                    mExpanded = true;

                /**
                    *Note* IDE's will report this condition as always true -- ignore it.
                    If the saved expanded position is not the same as the position at the time of this click, hide the
                    old saved expansion, show the current expansion, set expanded true set the current expansion as
                    the new saved expansion, and set the saved expanded position as the position at the time of this click
                 */
                } else if(mExpandedPos != position) {
                    mStub.setVisibility(View.GONE);
                    stub.setVisibility(View.VISIBLE);
                    mExpanded = true;
                    mStub = stub;
                    mExpandedPos = position;
                }
            }
        });
        if(mEntry.getDesc().equals("")) {
            desc.setText("Nutrition Entry");
        } else {
            desc.setText(WordUtils.titleCase(mEntry.getDesc().toLowerCase()));
        }

        if(mEntry.getServing().equals("")) {
            serving.setVisibility(View.GONE);
        } else {
            serving.setText(mEntry.getServing());
        }

        //Not final text; hardcoded strings won't be present in final
        if(mEntry.getCalories() == -1) {
            calories.setVisibility(View.GONE);
        } else {

            calories.setText(String.valueOf(mEntry.getCalories()) + " kcal");
        }

        if(mEntry.getProtein() == -1) {
            protein.setVisibility(View.GONE);
        } else {
            protein.setText(String.valueOf(mEntry.getProtein()) + " g of Protein");
        }

        if((serving.getVisibility() == View.GONE) && (calories.getVisibility() == View.GONE)
                && (protein.getVisibility() == View.GONE)) {
            noDetails.setVisibility(View.VISIBLE);
        }

        return view;
    }

}
