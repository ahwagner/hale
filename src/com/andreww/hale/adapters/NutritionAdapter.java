package com.andreww.hale.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.andreww.hale.R;
import com.andreww.hale.objects.NutritionEntry;
import com.andreww.hale.utils.WordUtils;

import java.util.List;

/**
 * Author: Andrew Wagner
 * Project: Hale v2
 * Date: 8/20/13
 * Time: 10:48 PM
 */
public class NutritionAdapter extends ArrayAdapter<NutritionEntry> {
    protected Context mContext;
    protected List<NutritionEntry> mEntries;
    protected TextView mDesc, mServing, mCalories, mProtein;
    protected static NutritionEntry mEntry;

    public NutritionAdapter(Context context, List<NutritionEntry> objects) {
        super(context, R.layout.nutrition_card_item, objects);
        this.mContext = context;
        this.mEntries = objects;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if(view == null){
            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = inflater.inflate(R.layout.nutrition_card_item, null);
        }

        mEntry = mEntries.get(position);
        mDesc = (TextView) view.findViewById(R.id.desc);
        mServing = (TextView) view.findViewById(R.id.serving);
        mCalories = (TextView) view.findViewById(R.id.calories);
        mProtein = (TextView) view.findViewById(R.id.protein);
        mDesc.setText(WordUtils.titleCase(mEntry.getDesc().toLowerCase()));
        mServing.setText(mEntry.getServing());
        mCalories.setText(String.valueOf(mEntry.getCalories()));
        mProtein.setText(String.valueOf(mEntry.getProtein()));

        return view;
    }

    public static String getEntryData() {
        return WordUtils.titleCase(mEntry.getDesc().toLowerCase());
    }

}
