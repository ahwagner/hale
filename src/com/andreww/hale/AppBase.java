package com.andreww.hale;

import android.app.Application;
import com.andreww.hale.objects.NutritionEntry;
import com.andreww.hale.objects.NutritionLogEntry;
import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Author: Andrew Wagner
 * Project: Hale v2
 * Date: 8/22/13
 * Time: 12:37 AM
 */
public class AppBase extends Application {
    public static final String PARSE_APP_ID="XxwJahKiCwhGZJE9IfFXBXDeBQZGttp9QKoxAf7q";
    public static final String PARSE_CLIENT_KEY="sOJayN9mz3JJV6qD8t5LGuaUsaRCB1AgTxi64HtB";

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this, PARSE_APP_ID, PARSE_CLIENT_KEY);
        ParseObject.registerSubclass(NutritionEntry.class);
        ParseObject.registerSubclass(NutritionLogEntry.class);
        ParseUser.enableAutomaticUser();
    }
}
