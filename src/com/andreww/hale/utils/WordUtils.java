package com.andreww.hale.utils;

/**
 * Author: Andrew Wagner
 * Project: Hale v2
 * Date: 8/21/13
 * Time: 2:01 AM
 */
public class WordUtils {

    public static String titleCase(String s) {
        String n = String.valueOf(s);
        if(n.contains(",")) {
            String[] sa = n.split(",");
            for(int i = 0; i < sa.length; i++) {
                if(i != (sa.length -1)) {
                    sa[i] = (sa[i] + ", ");
                }
                if(i == 0) {
                    n = sa[i];
                } else {
                    n = n + sa[i];
                }
            }

        }
        if(n.contains("&")) {
            String[] sa = n.split("&");
            for(int i = 0; i < sa.length; i++) {
                if(i != (sa.length - 1)) {
                    if(i == 0) {
                        sa[i] = (sa[i] + " &");
                    } else {
                        sa[i] = (" " + sa[i] + " &");
                    }
                } else {
                    sa[i] = (" " + sa[i]);
                }
                if(i == 0) {
                    n = sa[i];
                } else {
                    n = n + sa[i];
                }
            }

        }
        if(n.contains("/")) {
            String[] sa = n.split("/");
            for(int i = 0; i < sa.length; i++) {
                if(i != (sa.length -1)) {
                    sa[i] = (sa[i] + "/ ");
                }
                if(i == 0) {
                    n = sa[i];
                } else {
                    n = n + sa[i];
                }
            }

        }
        char[] ca = n.toCharArray();
        for(int i = 0; i < ca.length; i++) {
            if(i == 0) {
                ca[i] = Character.toUpperCase(ca[i]);
            } else if(Character.isSpaceChar(ca[i]) && (i != (ca.length - 1))) {
                ca[i+1] = Character.toUpperCase(ca[i+1]);
            } else if((ca[i] == '-') && (i != (ca.length - 1))) {
                ca[i+1] = Character.toUpperCase(ca[i+1]);
            } else if((ca[i] == '(') && (i != (ca.length - 1))) {
                ca[i+1] = Character.toUpperCase(ca[i+1]);
            }
        }
        return String.valueOf(ca);
    }

}
