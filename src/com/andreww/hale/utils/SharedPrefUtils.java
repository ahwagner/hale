package com.andreww.hale.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import com.parse.ParseUser;

import java.util.Calendar;

/**
 * Author: Andrew Wagner
 * Project: Hale
 * Date: 8/23/13
 * Time: 8:54 PM
 */
public class SharedPrefUtils {

    public static final int DAILY_API_LIMIT = 32;
    public static final int HOURLY_API_LIMIT = 1;
    public static final String DEV_USERNAME = "andoird213";

    public static boolean isUserLoggedIn(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("logged_in", false);
    }

    public static void setUserLoggedIn(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("logged_in", true);
        editor.commit();
    }

    public static void increaseApiCalls(FragmentActivity fa, Calendar now) {
        SharedPreferences sharedPreferences =  fa.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        int currentApiCalls = sharedPreferences.getInt("api_calls_counter", 0);
        editor.putInt("api_calls_counter", (currentApiCalls + 1));
        int year = now.get(Calendar.YEAR);
        int dayOfYear = now.get(Calendar.DAY_OF_YEAR);
        String checkCal = String.valueOf(dayOfYear) + String.valueOf(year);
        if(!isCurrentDay(fa, now) && isUserLoggedIn(fa)) {
            editor.putString("track_current_day", checkCal);
            editor.putInt("api_calls_counter", 0);
        }
        editor.commit();
    }

    public static boolean hasHitApiLimit(FragmentActivity fa) {
        if((!ParseUser.getCurrentUser().getUsername().equals(DEV_USERNAME)) && (getApiCalls(fa) > DAILY_API_LIMIT)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isFirstDailyApiCall(FragmentActivity fa) {
        if(getApiCalls(fa) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static int getApiCalls(FragmentActivity fa) {
        SharedPreferences sharedPreferences =  fa.getPreferences(Context.MODE_PRIVATE);
        return sharedPreferences.getInt("api_calls_counter", 0);
    }

    public static int getCurrentDay(FragmentActivity fa) {
        SharedPreferences sharedPreferences = fa.getPreferences(Context.MODE_PRIVATE);
        return sharedPreferences.getInt("track_current_day", 0);
    }

    public static boolean isCurrentDay(FragmentActivity fa, Calendar now) {
        SharedPreferences sharedPreferences =  fa.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        int year = now.get(Calendar.YEAR);
        int dayOfYear = now.get(Calendar.DAY_OF_YEAR);
        String checkCal = String.valueOf(dayOfYear) + String.valueOf(year);
        String currentDay = sharedPreferences.getString("track_current_day", checkCal);
        if(!checkCal.equals(currentDay)) {
            return false;
        } else {
            return true;
        }
    }

}
