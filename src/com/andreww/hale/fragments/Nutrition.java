package com.andreww.hale.fragments;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.afollestad.cardsui.CardAdapter;
import com.afollestad.cardsui.CardListView;
import com.andreww.hale.R;
import com.andreww.hale.activities.Main;
import com.andreww.hale.dialogs.NewNutritionLogDialog;
import com.andreww.hale.objects.NutritionEntry;
import com.andreww.hale.objects.NutritionLogEntry;
import com.andreww.hale.utils.SharedPrefUtils;
import com.parse.*;

import java.util.Calendar;
import java.util.List;

/**
 * Author: Andrew Wagner
 * Project: Hale v2
 * Date: 8/17/13
 * Time: 6:46 PM
 */
public class Nutrition extends Fragment {

    public LinearLayout mLayout;
    public CardListView mNutritionEntries;
    public CardAdapter<NutritionLogEntry> mAdapter;
    public ProgressBar mLoading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout =  inflater.inflate(R.layout.nutrition, container, false);

        mNutritionEntries = (CardListView) layout.findViewById(R.id.nutrition_entries_listview);
        mAdapter = new CardAdapter<NutritionLogEntry>(getActivity());
        mAdapter.setCardLayout(R.layout.list_item_card);
        mAdapter.setAccentColor(Color.parseColor("#CC0000"));
        mNutritionEntries.setAdapter(mAdapter);

        mLayout = (LinearLayout) layout.findViewById(R.id.nutrition_root);

        mLoading = new ProgressBar(getActivity());
        mLoading.setIndeterminate(true);

        final ParseQuery<NutritionEntry> search = Main.getCachedSearch();
        List<NutritionEntry> entries;
        try {
            if(search.getCachePolicy() == ParseQuery.CachePolicy.CACHE_ONLY) {
                entries = search.find();
                Toast.makeText(getActivity(), "DEBUG: Successfully retrieved cached search results", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "DEBUG: Relying on network, not cache", Toast.LENGTH_SHORT).show();
            }
        } catch(ParseException e) {
            Toast.makeText(getActivity(), "DEBUG: Unknown error", Toast.LENGTH_SHORT).show();
        }

        updateData(false);

        return layout;
    }

    public void launchNewNutritionEntryDialog() {
        final NewNutritionLogDialog builder = new NewNutritionLogDialog(getActivity());
        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                NutritionLogEntry entry = NutritionLogEntry.newEntry(
                        builder.getDesc(),
                        builder.getServing(),
                        builder.getServingsConsumed(),
                        builder.getCalories(),
                        builder.getProtein()
                );
                showLoading();
                entry.setACL(new ParseACL(ParseUser.getCurrentUser()));
                entry.saveEventually(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        SharedPrefUtils.increaseApiCalls(getActivity(), Calendar.getInstance());
                        updateData(true);
                    }
                });
            }
        });
        builder.create();
        builder.show();
    }

    public void showLoading() {
        mLayout.removeViewAt(0);
        mLayout.addView(mLoading, 0);
    }



    public void updateData(boolean newData) {
        showLoading();
        final ParseQuery<NutritionLogEntry> query = new ParseQuery("NutritionLog");
            query.setMaxCacheAge(86400000);
            if(!query.hasCachedResult() || newData) {
                query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
                Toast.makeText(getActivity(), "DEBUG: Reading network log entries", Toast.LENGTH_SHORT).show();
            } else {
                query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ONLY);
                Toast.makeText(getActivity(), "DEBUG: Reading cached log entries", Toast.LENGTH_SHORT).show();
            }
            query.orderByDescending("createdAt");
            query.findInBackground(new FindCallback<NutritionLogEntry>() {

                @Override
                public void done(List<NutritionLogEntry> entries, ParseException error) {
                    /** Begin DEBUG code */
                    if (entries != null) {
                        if(query.getCachePolicy() != ParseQuery.CachePolicy.CACHE_ONLY)
                            SharedPrefUtils.increaseApiCalls(getActivity(), Calendar.getInstance());
                        if (SharedPrefUtils.isCurrentDay(getActivity(), Calendar.getInstance())) {
                            Toast.makeText(getActivity(),
                                    "DEBUG: Current day; keeping daily API calls",
                                    Toast.LENGTH_SHORT).show();
                            Toast.makeText(getActivity(),
                                    "DEBUG: You've made " +
                                            String.valueOf(SharedPrefUtils.getApiCalls(getActivity())) + " API calls today",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(),
                                    "DEBUG: New day; resetting daily API calls",
                                    Toast.LENGTH_SHORT).show();
                            Toast.makeText(getActivity(),
                                    "DEBUG: You've made " +
                                            String.valueOf(SharedPrefUtils.getApiCalls(getActivity())) + " API calls today",
                                    Toast.LENGTH_SHORT).show();
                        }
                        /** End DEBUG code */
                        updateList(entries);
                    }
                }
            });

    }

    public  void updateList(List<NutritionLogEntry> entries) {
        mAdapter.clear();
        mAdapter.add(entries);
        mLayout.removeViewAt(0);
        mLayout.addView(mNutritionEntries, 0);
    }

}
