package com.andreww.hale.objects;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Author: Andrew Wagner
 * Project: Hale
 * Date: 8/25/13
 * Time: 1:01 AM
 */
public class FoObject extends JSONObject {

    public FoObject(String name, int age) {
        try {
            put("name", name);
            put("age", age);
        } catch(JSONException e) {

        }
    }

    public String getName() {
        try{
            return getString("name");
        } catch(JSONException e) {
            return null;
        }
    }

    public int getAge() {
        try{
            return getInt("age");
        } catch(JSONException e) {
            return -1;
        }
    }
}
