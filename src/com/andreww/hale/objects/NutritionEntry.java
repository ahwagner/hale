package com.andreww.hale.objects;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Author: Andrew Wagner
 * Project: Hale v2
 * Date: 8/20/13
 * Time: 10:52 PM
 */

@ParseClassName("USDADB")
public class NutritionEntry extends ParseObject {
    public static final String DESC = "Shrt_Desc";
    public static final String SERVING = "GmWt_Desc1";
    public static final String CALORIES = "Energ_Kcal";
    public static final String PROTEIN =  "proteinG";
    public static final String USER_ENTRY = "isUserEntry";

    public NutritionEntry() {

    }

    public String getDesc() {
        return getString(DESC);
    }

    public String getServing() {
        return getString(SERVING);
    }

    public int getCalories() {
        return getInt(CALORIES);
    }

    public int getProtein() {
        return getInt(PROTEIN);
    }

    public boolean isUserEntry() {
        return getBoolean(USER_ENTRY);
    }

    public void newEntry(String desc, String serving, int calories, int protein) {
        put(DESC, desc);
        put(SERVING, serving);
        put(CALORIES, calories);
        put(PROTEIN, protein);
        put(USER_ENTRY, true);
        saveEventually();
    }
}
