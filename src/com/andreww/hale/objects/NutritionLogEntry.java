package com.andreww.hale.objects;

import android.graphics.drawable.Drawable;
import com.afollestad.cardsui.Card;
import com.afollestad.cardsui.CardBase;
import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Author: Andrew Wagner
 * Project: Hale v2
 * Date: 8/21/13
 * Time: 11:12 PM
 */
@ParseClassName("NutritionLog")
public class NutritionLogEntry extends ParseObject implements CardBase {
    public static final String DESC = "Shrt_Desc";
    public static final String SERVING = "GmWt_Desc1";
    public static final String  SERVINGS_CONSUMED = "Servings_Consumed";
    public static final String CALORIES = "Energ_Kcal";
    public static final String PROTEIN =  "proteinG";

    public String getDesc() {
        return getString(DESC);
    }

    public String getServing() {
        return getString(SERVING);
    }

    public int getServingsConsumed() {
        return getInt(SERVINGS_CONSUMED);
    }

    public int getCalories() {
        return getInt(CALORIES);
    }

    public int getProtein() {
        return getInt(PROTEIN);
    }

    @Override
    public String getTitle() {
        return getDesc();
    }

    @Override
    public String getContent() {
        return getServing() + "\n" + String.valueOf(getCalories()) + "\n" + String.valueOf(getProtein());
    }

    @Override
    public boolean isHeader() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isClickable() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getPopupMenu() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Card.CardMenuListener getPopupListener() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Drawable getThumbnail() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public NutritionLogEntry() {

    }

    @Override
    public boolean shouldIgnore() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getLayout() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object getTag() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public static NutritionLogEntry newEntry(String desc, String serving, int servingsConsumed, int calories, int protein) {
        NutritionLogEntry entry = new NutritionLogEntry();
        entry.put(DESC, desc);
        entry.put(SERVING, serving);
        entry.put(SERVINGS_CONSUMED, servingsConsumed);
        entry.put(CALORIES, calories);
        entry.put(PROTEIN, protein);
        return entry;
    }

    @Override
    public boolean isSameAs(Object another) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
